#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdarg.h>
#include <sys/stat.h>

using namespace std;

void help() {
    printf("Dump dll file to lib file. Version 1.00 author: laizx@sina.com\n");
    printf("Usage: dumplib [-h] dllFile [libFile]\n");
    printf(
            "\t[libFile] default: dllFile.lib\n"
            "\t-m|--machine: {ARM|ARM64|EBC|X64|X86}, default: X64\n"
            "\t-h|--help print this help message\n"
    );
    exit(0);

}
inline bool existsFile (const std::string& name) {
    struct stat buffer;

    return (stat (name.c_str(), &buffer) == 0);
}

void changeFileSuffix(string &fileName, const string suffix) {
    char *ext=strrchr(fileName.c_str(),'.');

    if( ext )
    {
        ext++;
        strcpy(ext,suffix.c_str());
    }
}

bool productDefFile(string inFile,string outFile) {
    FILE *fpIn,*fpOut;

    if( (fpIn=fopen(inFile.c_str(),"rt")) == NULL) {
        printf("fopen file[%s] error!\n",inFile.c_str());
        return false;
    }

    if( (fpOut=fopen(outFile.c_str(),"wt")) == NULL) {
        printf("fopen file[%s] error!\n",outFile.c_str());
        fclose(fpIn);
        return false;
    }
    fprintf(fpOut,"LIBRARY\n\n");
    fprintf(fpOut,"EXPORTS\n");

    char szBuf[512];
    char szBegin[] = "    ordinal hint RVA      name";
    char szEnd[] = "  Summary";
    int lenBegin = strlen(szBegin);
    int lenEnd = strlen(szEnd);
    int status = 0;

    char szFuncName[512];
    int len;

    bool loop = true;
    while(loop) {
        memset(szBuf,0, sizeof(szBuf));

        if( fgets(szBuf, 512, fpIn) == NULL ) {
            break;
        }
        switch (status) {
            case 0:
                if( memcmp(szBuf,szBegin,lenBegin) == 0 ) {
                    status = 1;
                    break;
                }
                break;
            case 1:
                if( memcmp(szBuf,szEnd,lenEnd) == 0 ) {
                    loop = false;
                    break;
                }
                len = strlen(szBuf);
                if( len < 26 )
                    continue;

                memset(szFuncName,0 ,sizeof(szFuncName));
                strncpy(szFuncName,&szBuf[26], len - 26);
                if( szFuncName[0] == '_' )
                    continue;
                fprintf(fpOut,"\t%s",szFuncName);

                break;
        }
    }

    fclose(fpIn);
    fclose(fpOut);

    return true;
}
int main(int argc,char **argv) {
    string machine = "x64";

    string options = "h";
    struct option longOptions[] = {
            {"machine",required_argument,NULL,'m'},
            {"help",no_argument,NULL,'h'},
            {NULL,0,NULL,0}
    };

    int nextOpt;
    while ((nextOpt = getopt_long(argc, argv, options.c_str(),longOptions,NULL)) != EOF)
    {
        switch (nextOpt) {
            case 'h':
                help();
                break;
            default:
                printf("Wrong command! [%d]\n",nextOpt);
                exit(-1);
        }
    }
    if( argc < 2 ) {
        help();
        return -1;
    }

    string dllFile = argv[optind++];
    string defFile;
    string tmpFile;
    string libFile;

    if( optind < argc ) {
        libFile = argv[optind++];
    } else {
        libFile = dllFile;
        changeFileSuffix(libFile,"lib");
    }
    defFile = libFile;
    changeFileSuffix(defFile,"def");
    tmpFile = defFile;
    changeFileSuffix(tmpFile,"tmp");

//    printf("%s\n%s\n%s\n%s\n",dllFile.c_str(),libFile.c_str(),defFile.c_str(),tmpFile.c_str());

    if( !existsFile(dllFile) ) {
        printf("File %s not exist!\n", dllFile.c_str());
        return -1;
    }

    //检查执行文件是否存在
    string dumpbinFile = "vender\\dumpbin.exe";
    string libexeFile = "vender\\lib.exe";
    string linkFile = "vender\\link.exe";
    string mspFile = "vender\\mspdb140.dll";

    if( !existsFile(dumpbinFile) ) {
        printf("File %s not exist!\n", dumpbinFile.c_str());
        return -1;
    }
    if( !existsFile(libexeFile) ) {
        printf("File %s not exist!\n", libexeFile.c_str());
        return -1;
    }
    if( !existsFile(linkFile) ) {
        printf("File %s not exist!\n", linkFile.c_str());
        return -1;
    }
    if( !existsFile(mspFile) ) {
        printf("File %s not exist!\n", mspFile.c_str());
        return -1;
    }

    //第一步，使用dumpbin.exe生成def文件
    char cmd[512];
    sprintf(cmd,"%s /exports %s > %s", dumpbinFile.c_str(), dllFile.c_str(), tmpFile.c_str());
//    printf("%s\n",cmd);

    if( system(cmd) < 0 ) {
        printf("system %s error!\n",cmd);
        return -1;
    }

    //第二步，生成标准的def文件
    if( productDefFile(tmpFile,defFile) == false ) {
        return -1;
    }

    //第三步，使用lib.exe生成lib文件
    sprintf(cmd,"%s /def:%s /machine:%s /out:%s ",
            libexeFile.c_str(),
            defFile.c_str(),
            machine.c_str(),
            libFile.c_str());
//    printf("%s\n",cmd);

    if( system(cmd) < 0 ) {
        printf("system %s error!\n",cmd);
        return -1;
    }

    printf("\n\n%s file ok!\n", libFile.c_str());
    return 0;
}
