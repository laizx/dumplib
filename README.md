# dumplib

#### 介绍
从dll文件生成lib文件

#### 使用说明
1.  在windows环境变量中增加 dumplib.exe 所在路径；
2.  进入要转换的dll所在路径；
3.  执行 dumplib xxx.dll xxx.lib 即可生成相应的lib文件。

#### 注意
vender目录存放的是所需要的visual studio 15.0的exe和dll文件，必须放到dumplib.exe所在的目录下